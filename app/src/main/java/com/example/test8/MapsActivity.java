package com.example.test8;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentActivity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.SearchView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, TaskLoadedCallback{

    private static final String TAG = null;
    private GoogleMap mMap;
    double lat,lon;
    ImageButton mapView,btnGetLoc,getWay,car,mapTopmap,sputnik;

    //рисуем линию
    private MarkerOptions place1, place2;
    private Polyline currentPolyline;
    //
    private View mapViews;

    //если что удалить
    SupportMapFragment mapFragment;
    SearchView searchView;

    List<LatLng> latLngList = new ArrayList<>();
    List<Marker> markerList = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        //добавляем кнопку поиска позиции и выставляем позиции

        //place1 = new MarkerOptions().position(new LatLng(27.658143, 85.3199503)).title("Location 1");
        //place2 = new MarkerOptions().position(new LatLng(27.667491, 85.3208583)).title("Location 2");

        //удалить если что
        searchView = findViewById(R.id.sv_location);


        //Кнопка запоминания позиции автомобиля
        sputnik = (ImageButton) findViewById(R.id.sputnik);
        mapTopmap = (ImageButton) findViewById(R.id.mapTopmap);
        btnGetLoc = (ImageButton) findViewById(R.id.btnGetLoc);
        car = (ImageButton) findViewById(R.id.car);
        mapView = (ImageButton) findViewById(R.id.mapView);
        getWay = (ImageButton) findViewById(R.id.getWay);
        ActivityCompat.requestPermissions(MapsActivity.this,new String[]{Manifest.permission.ACCESS_FINE_LOCATION},123);


        btnGetLoc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GPStracer g = new GPStracer(getApplicationContext());
                Location l = g.getLocation();


                //проверка доступность координат
                if (l != null){

                    //получаем данные долготы и широты
                    lat = l.getLatitude();
                    lon = l.getLongitude();

                    place1 = new MarkerOptions().position(new LatLng(lat, lon)).title("Location 1");
                    mMap.addMarker(place1);

                }else{

                    //Вывод сообщения об отсутствии сигнала
                    Toast.makeText(getApplicationContext(),"GPS сигнал отсутствует", Toast.LENGTH_LONG).show();
                }
            }
        });


        //switch map/sputnik
        mapTopmap.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                    mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

            }

        });

        sputnik.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                    mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);

            }

        });


        car.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GPStracer g = new GPStracer(getApplicationContext());
                Location l = g.getLocation();


                //проверка доступность координат
                if (l != null) {

                    //получаем данные долготы и широты
                    lat = l.getLatitude();
                    lon = l.getLongitude();

                    place2 = new MarkerOptions().position(new LatLng(lat, lon)).title("Location 2");
                    mMap.addMarker(place2);
                    new FetchURL(MapsActivity.this).execute(getUrl(place1.getPosition(), place2.getPosition(), "walking"), "walking");


                } else {

                    //Вывод сообщения об отсутствии сигнала
                    Toast.makeText(getApplicationContext(), "GPS сигнал отсутствует", Toast.LENGTH_LONG).show();
                }
            }

        });



        //перенаправление камеры на авто
        getWay.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                //вывод диалогового окна подтверждения удаления
                //Alert
                AlertDialog.Builder builder= new AlertDialog.Builder(MapsActivity.this);
                //заменить переменной настроек, языковое расширение
                builder.setMessage("Удалить все сохраненные позиции?").setPositiveButton("Да", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        //Очиска всех маркеров
                        //for (Marker marker : markerList) marker.remove();
                        //latLngList.clear();
                        //markerList.clear();
                        mMap.clear();

                    }
                }).setNegativeButton("Нет",null);
                AlertDialog alertDialog = builder.create();
                alertDialog.show();

            }
        });


        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.mapTop);

        //удалить если что
        //поиск городов
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                String location = searchView.getQuery().toString();
                List<Address> addressList = null;

                if (location != null || !location.equals("")){
                    Geocoder geocoder = new Geocoder(MapsActivity.this);
                    try {
                        addressList = geocoder.getFromLocationName(location,1);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    Address address = addressList.get(0);
                    LatLng latLng = new LatLng(address.getLatitude(), address.getLongitude());
                    //mMap.addMarker(new MarkerOptions().position(latLng).title(location));
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 18));
                }

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        mapFragment.getMapAsync(this);
        mapViews = mapFragment.getView();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }


    @Override
    @SuppressWarnings("MissingPermission")
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;


        //стиль карты
        try {
            // Customise the styling of the base map using a JSON object defined
            // in a raw resource file.
            boolean success = googleMap.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(
                            this, R.raw.style_json));

            if (!success) {
                Log.e(TAG, "Style parsing failed.");
            }
        } catch (Resources.NotFoundException e) {
            Log.e(TAG, "Can't find style. Error: ", e);
        }

        GPStracer g = new GPStracer(getApplicationContext());
        Location l = g.getLocation();
        if (l != null) {
        LatLng myPos = new LatLng(l.getLatitude(), l.getLongitude());
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(myPos, 18));
    } else {

        //Вывод сообщения об отсутствии сигнала
        Toast.makeText(getApplicationContext(), "GPS сигнал отсутствует", Toast.LENGTH_LONG).show();
    }


        mMap.setMyLocationEnabled(true);
        mMap.getUiSettings().setMyLocationButtonEnabled(true);
        mMap.getUiSettings().setZoomControlsEnabled(true);

        //переопределение положения кнопки навигации
        if (mapViews != null && mapViews.findViewById(Integer.parseInt("1")) != null) {
            View locationButton = ((View) mapViews.findViewById(Integer.parseInt("1")).getParent())
                    .findViewById(Integer.parseInt("2"));
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) locationButton.getLayoutParams();
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
            layoutParams.setMargins(0, 0, 40, 750);
        }
        //переопределение положения зума
        if (mapViews != null && mapViews.findViewById(Integer.parseInt("1")) != null) {
            View zoomIn = ((View) mapViews.findViewById(Integer.parseInt("1")).getParent())
                    .findViewById(Integer.parseInt("1"));
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) zoomIn.getLayoutParams();
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
            layoutParams.setMargins(0, 0, 40, 300);
        }
        //компас
        if (mapViews != null && mapViews.findViewById(Integer.parseInt("1")) != null) {
            View compass = ((View) mapViews.findViewById(Integer.parseInt("1")).getParent())
                    .findViewById(Integer.parseInt("5"));
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) compass.getLayoutParams();
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
            layoutParams.setMargins(0, 0, 40, 300);
        }
    }


    //получение значений json, рисуем
    private String getUrl(LatLng origin, LatLng dest, String directionMode) {
        // Origin of route
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;
        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;
        // Mode
        String mode = "mode=" + directionMode;
        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + mode;
        // Output format
        String output = "json";
        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters + "&key=" + getString(R.string.map_key);
        return url;
    }

    @Override
    public void onTaskDone(Object... values) {
        if (currentPolyline != null)
            currentPolyline.remove();
        currentPolyline = mMap.addPolyline((PolylineOptions) values[0]);
    }

}
